# Playbook de bootstrap d'un serveur Debian 
Ce dépôt contient un playbook Ansible permettant de bootstraper un serveur Debian après une installation "from scratch"

## Pré-requis
Pour utiliser ce playbook, il faut :
* Le mot de passe de l'utilisateur qui exécutera les tâches Ansible ou sa clef SSH
* Un serveur Debian fraichement installé : ```Debian GNU/Linux 8.7 (jessie)```
* Ansible version ```>= 2.3.0.0```
* Python version ```>= 2.7.12```

## Dépendances
Aucune

## Liste des machines disponibles
Se référer à la liste présente dans l'inventory
```
├── inventory
│   └── hosts
```

## Fonctionnement du playbook
Ce playbook Ansible est là pour simplifier/faciliter le bootstrap d'une machine fraichement livré par votre fournisseur.  
Le tout en suivant le principe suivant : ```une ligne de commande = une distribution fonctionnelle et personnalisée```

### Liste et description des tâches qui peuvent être exécutées
* Créer les utilisateurs et les groupes déclarés dans ```group_vars/users.yml``` (attention le fichier est chiffré dans le dépôt, il faut créer le votre)
* Regénérer la configuration des locales de la distribution
* Personnaliser les sources.list de la machine
* Mettre à jour le cache APT
* Supprimer les paquets systèmes déclarés comme inutile dans ```group_vars/package_list.yml```
* Installer les paquets systèmes déclarés comme nécessaires dans ```group_vars/package_list.yml```
* Installer et configurer le service Docker
* Checkouter le dépôt GitLab qui sera utilisé pour les tâches de "personnalisation" de la distribution : [sysadmin-tools repository on GitLab](https://gitlab.com/jlecorre/sysadmin-tools)
* Générer une configuration propre pour le système :
  > Configurer Bash et son prompt  
  > Personnaliser Vim  
  > Personnaliser Git (```.gitignore``` et ```.gitconfig```)  
  > Personnaliser la configuration de Screen si nécessaire  
  > Etc...
* Déployer le script de MOTD dynamique hébergé ici : [sysadmin-tools repository on GitLab](https://gitlab.com/jlecorre/sysadmin-tools)
* Générer les alias du système
* Déployer une configuration avancée de Fail2ban et démarrer le service  
  > #TODO - Extraire cette tâche du playbook pour en faire un rôle
* Installer un environnement basque LAMP (Linux Apache MySQL PHP)
* Sécuriser la configuration du système en modifiant le fichier ```/etc/sysctl.conf```
* Installer et configurer le programme LogWatch
* Plus si affinitée ... =)

### Liste des tags disponibles
En utilisant le système des "tags" intégré à Ansible, il est possible de n'exécuter que les "tasks" qui vous intéresse !  
Pour les utiliser, voir la commande décrite au paragraphe "Exécution du playbook".

Liste des tags disponibles :
> users  
> apt  
> docker  
> bash  
> vim  
> git  
> motd  
> aliases  
> f2b  
> lamp  
> sysctl  
> logwatch  

## Vérification de la syntaxte du playbook
Pour lancer une analyse de la syntaxe des fichiers YAML composant le playbook :
```
ansible-playbook -i inventory/hosts --syntax-check bootstrap_debian_server.yml
```

## Exécution du playbook
```
ansible-playbook -i inventory/hosts bootstrap_debian_server.yml -e "host_to_deploy=${group_of_hosts}" [--tags "${your_tags}"] [--ask-pass] [--vault-password-file /path/to/passphrase] [-vvv]
```
Avec les paramètres suivants :  
```
-e "host_to_deploy=${group_of_hosts}" => la machine ou le groupe de serveurs à bootstraper  
[--tags "${your_tags}"] => un tag disponible pour cibler la ou les tâche(s) à exécuter  
[--ask-pass] => paramètre utile lorsque l'on ne dispose pas de clef SSH pour la machine distante  
[--vault-password-file /path/to/passphrase] => paramètre utile pour ne pas avoir à renseigner la passphrase Vault à chaque exécution du playbook  
[-vvv] => permet d'activer le mode "verbeux" d'Ansible
```

## Tests du playbook
Pour tester un playbook Ansible, il est possible d'utiliser Vagrant [vagrant](https://www.vagrantup.com/).
Le répertoire ```tests``` contient le nécessaire pour démarrer une box Vagrant et exécuter le playbook Ansible en local.
```
└── tests
    └── Vagrantfile
```

Commandes utiles à connaitre :  
```
vagrant up => démarre une box Vagrant et exécute le playbook Ansible en local (voir le fichier inventory/hosts pour la configuration SSH d'Ansible)
vagrant provision => rejoue le playbook Ansible dans une box Vagrant déjà démarrée
vagrant destroy => supprime définitivement une box Vagrant
ssh -i ${HOME}/.vagrant.d/insecure_private_key vagrant@<ip_vagrant_box> => permet de se connecter en SSH à une boxe démarrée (pratique pour aller voir ce qu'il se passe dedans :)
```

## Informations supplémentaires
* Penser à configurer une clef SSH avant d'utiliser les tags qui exécutent un ```git clone``` sur un dépôt de code "protégé"
* Permissions nécessaire à la protection de la clef SSH : ```chmod 0600 file_name```

## Informations concernant l'auteur
Joël LE CORRE <contact[@]sublimigeek.fr>
